# R.A.T.S debian package

DEBIAN package for the project https://code.google.com/archive/p/rough-auditing-tool-for-security/

## (re) Generation of rats_2.4.* files
```shell
curl -ksLo rats_2.4.orig.tar.gz https://storage.googleapis.com/google-code-archive-downloads/v2/code.google.com/rough-auditing-tool-for-security/rats-2.4.tgz
(cd rats/ && debuild -S -sa --lintian-opts -i)
```
