
ubuntu_is_installed() {
    if [ "$(dpkg -s "$1" 2>/dev/null | grep -o 'installed' | head -n 1)" == "installed" ]; then
        return 0
    fi
    return 1
}

if [ $(id -u) -eq 0 ]; then
    _sudo() {
        "$@"
    }
else
    if ! which sudo > /dev/null 2>&1; then
        print_critical "In user mode, sudo with suitable system rights is required"
    fi
    _sudo() {
        sudo "$@"
    }
fi

# remove old versions...
if ubuntu_is_installed docker.io; then
    print_note "Removing old docker.io package..."
    _sudo /etc/init.d/docker stop
    _sudo apt-get remove -y docker.io
fi
if ubuntu_is_installed docker-engine; then
    print_note "Removing old docker-engine package..."
    _sudo /etc/init.d/docker stop
    _sudo apt-get remove -y docker-engine
fi
if grep -qr 'download.docker.com' /etc/apt/; then
    print_note "Docker apt repository is already present."
else
    print_note "Adding docker apt repository..."
    # setup - pre install
    _sudo apt-get update
    _sudo apt-get install -y aufs-tools \
        linux-image-extra-virtual \
        apt-transport-https \
        ca-certificates \
        curl \
        software-properties-common
    # add docker repo
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | _sudo apt-key add -
    _sudo add-apt-repository \
        "deb [arch=amd64] http://download.docker.com/linux/ubuntu \
        $(lsb_release -cs) \
        stable"
fi
_sudo apt-get update
_sudo apt-get -y install docker-ce \
    gawk \
    python-yaml \
    moreutils \
    jq

if [ "${USER}" != "root" ]; then
    _sudo gpasswd -a ${USER} docker
fi

# Setup docker0 bridge with:
# - NET: 192.168.255.0/24 (by default)
# - DNS: Use system dns instead of Google one
_sudo ip link del docker0 2>/dev/null || true
_sudo mkdir -p /etc/docker
if [ ! -s /etc/docker/daemon.json ]; then
    _sudo bash -c 'echo {} > /etc/docker/daemon.json'
fi
for dns in $CITBX_DOCKER_DNS_LIST; do
    if [ -n "$dnslist" ]; then
        dnslist="$dnslist, \"$dns\""
    else
        dnslist="\"$dns\""
    fi
done

# Put in comment the docker default options
_sudo grep '^ *\<DOCKER_OPTS\>' /etc/default/docker &&
_sudo sed '/^ *\<DOCKER_OPTS\>/s/^/#/' -i /etc/default/docker

_sudo cat /etc/docker/daemon.json |
jq ". + {
\"bip\": \"$CITBX_DOCKER_BIP\",
\"fixed-cidr\": \"$CITBX_DOCKER_FIXED_CIDR\",
\"dns\": $(bashopts_dump_array "string" "${CITBX_DOCKER_DNS_LIST[@]}")
}" | _sudo sponge /etc/docker/daemon.json

# Add user SSL ROOT CA
if [ -d $CITBX_ABS_DIR/ca-certificates ]; then
    _sudo cp $CITBX_ABS_DIR/ca-certificates/.crt /usr/local/share/ca-certificates/
    _sudo update-ca-certificates
    _sudo mkdir -p /etc/docker/certs.d
    _sudo cp $CITBX_ABS_DIR/ca-certificates/.crt /etc/docker/certs.d/
fi
_sudo service docker restart
