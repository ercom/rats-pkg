#!/bin/bash -e
# citools4gitlab: CI tools for Gitlab
# Copyright (C) 2017 ERCOM - Emeric Verschuur <emeric@mbedsys.org>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Print an error message and exit with error status 1
print_critical() {
    >&2 printf "\e[91m[ERROR] %s\e[0m\n" "$@"
    exit 1
}

# Print an error message
print_error() {
    >&2 printf "\e[91m[ERROR] %s\e[0m\n" "$@"
}

# Print a warning message
print_warning() {
    >&2 printf "\e[93m[WARNING] %s\e[0m\n" "$@"
}

# Print a note message
print_note() {
    printf "[NOTE] %s\n" "$@"
}

# Pring an info message
print_info() {
    printf "\e[92m[INFO] %s\e[0m\n" "$@"
}

# Check bash
if [ ! "${BASH_VERSINFO[0]}" -ge 4 ]; then
    print_critical "This script needs BASH version 4 or greater"
fi

# ci-tools base directory bath
CITBX_ABS_DIR=$(dirname $(readlink -f $0))
# Extract project specific values
if [ -f $CITBX_ABS_DIR/citbx.properties ]; then
    . $CITBX_ABS_DIR/citbx.properties
fi
if [ -z "$CI_PROJECT_DIR" ]; then
    # Find the project directory
    CI_PROJECT_DIR=$(
        cd $CITBX_ABS_DIR
        while true; do
            if [ -f .git/HEAD ]; then
                echo $PWD
                break
            elif [ "$PWD" == "/" ]; then
                print_critical "Unable to find the project root directory"
            fi
            cd ..
        done
    )
fi
# Current job script relative directory path
CITBX_DIR=${CITBX_ABS_DIR#${CI_PROJECT_DIR}/}

citbx_register_handler() {
    local list="citbx_job_stage_${2}"
    local func="${1}_${2}"
    if [[ "$(type -t $func)" != "function" ]]; then
        return 0
    fi
    local pattern='\b'"$func"'\b'
    if [[ "${!list}" =~ $pattern ]]; then
        return 0
    fi
    case "$2" in
        define|setup|before|main)
            eval "${list}=\"${!list} $func\""
            ;;
        after)
            eval "${list}=\"$func ${!list}\""
            ;;
        *)
            print_critical "Use: citbx_register_handler <prefix> define|setup|before|main|after"
            ;;
    esac
}

declare -A CITBX_USE_LIST
# Add module in the use list
citbx_use() {
    local module=$1
    if [ -z "$module" ]; then
        print_critical "Usage: citbx_use <module_name>"
    fi
    if [ "${CITBX_USE_LIST[$module]}" == "true" ]; then
        return 0
    fi
    if [ ! -f "$CITBX_ABS_DIR/modules/${module}.sh" ]; then
        print_critical "Module ${module} not found!"
    fi
    . $CITBX_ABS_DIR/modules/${module}.sh
    CITBX_USE_LIST[$module]="true"
    for h in $module_handler_list; do
        citbx_register_handler "citbx_module_${module}" $h
    done
}

citbx_local() {
    if [ -f $CITBX_ABS_DIR/citbx.local ]; then
        . $CITBX_ABS_DIR/citbx.local
    fi
}

# Job end handler
citbx_job_finish() {
    local CITBX_EXIT_CODE=$?
    if [ "$CITBX_JOB_FINISH_CALLED" != "true" ]; then
        CITBX_JOB_FINISH_CALLED="true"
    else
        return 0
    fi
    for hook in $citbx_job_stage_after; do
        cd $CI_PROJECT_DIR
        $hook $CITBX_EXIT_CODE
    done
    if [ "$CITBX_EXIT_CODE" == "0" ]; then
        print_info "CI job success!"
    else
        print_error "CI job failure with exit code $CITBX_EXIT_CODE"
    fi
    print_note "Job execution time: $(date +"%H hour(s) %M minute(s) and %S second(s)" -ud @$(($(date +%s) - $CITBX_JOB_START_TIME)))"
}

# If running inside the suitable runner / on gitlab runner
if [ "$GITLAB_CI" == "true" ]; then
    # Load job
    citbx_local
    JOB_EXT_FILE_NAME=${JOB_EXT_FILE_NAME:-"$CI_JOB_NAME.sh"}
    module_handler_list="before after"
    JOB_EXT_FILE_PATH="$CITBX_ABS_DIR/run.d/${JOB_EXT_FILE_NAME}"
    if [ ! -f "$JOB_EXT_FILE_PATH" ]; then
        print_critical "Job definition file $JOB_EXT_FILE_PATH not found"
    fi
    . $JOB_EXT_FILE_PATH
    citbx_register_handler "job" "main"
    citbx_register_handler "job" "after"
    for hook in $citbx_job_stage_before; do
        cd $CI_PROJECT_DIR
        $hook
    done
    CITBX_JOB_START_TIME=$(date +%s)
    trap citbx_job_finish EXIT SIGINT SIGTERM
    print_info "CI job begin"
    if [ -z "$citbx_job_stage_main" ]; then
        print_critical "Funtion job_main not found in the file $JOB_EXT_FILE_PATH"
    fi
    for hook in $citbx_job_stage_main; do
        cd $CI_PROJECT_DIR
        $hook
    done
    exit 0
fi

# Force use citbx_run_ext_job to run another job
if [ "$CITBX" == "true" ]; then
    print_critical "You cannot call another CI script (i.e. other external job) into a CI script" \
        "Please use citbx_run_ext_job instead"
fi
export CITBX="true"

# YAML to JSON convertion
yaml2json() {
    cat "$@" | python -c 'import sys, yaml, json; json.dump(yaml.load(sys.stdin), sys.stdout)'
}

# Collect the missing binaries and other dependencies
CITBX_MISSING_PKGS=()
for bin in gawk jq dockerd; do
    if ! which $bin > /dev/null 2>&1; then
        CITBX_MISSING_PKGS+=($bin)
    fi
done
if [ "$(echo "true" | yaml2json 2>/dev/null)" != "true" ]; then
    CITBX_MISSING_PKGS+=("python-yaml")
fi

if [ ! -f $CI_PROJECT_DIR/.gitlab-ci.yml ]; then
    print_critical "$CI_PROJECT_DIR/.gitlab-ci.yml file not found"
fi
if [ ${#CITBX_MISSING_PKGS[@]} -eq 0 ]; then
    GITLAB_CI_JSON=$(yaml2json $CI_PROJECT_DIR/.gitlab-ci.yml)
else
    print_warning "System setup required (command $0 setup [arguments...])"
fi

gitlab_ci_query() {
    jq "$@" <<< "$GITLAB_CI_JSON"
}

# Check environment and run setup
citbx_check_env() {
    local os_id
    if [ "$1" != "true" ]; then
        if [ ${#CITBX_MISSING_PKGS[@]} -gt 0 ]; then
            print_critical "System setup needed (binary(ies)/component(s) '${CITBX_MISSING_PKGS[*]}' missing): please execute $0 --system-setup first"
        fi
        return 0
    fi
    if which lsb_release > /dev/null 2>&1; then
        os_id=$(lsb_release --id --short)
    elif [ -f /etc/os-release ]; then
        eval "$(sed 's/^NAME=/os_id=/;tx;d;:x' /etc/os-release)"
    fi
    local setupsh="$CITBX_ABS_DIR/env-setup/${os_id,,}.sh"
    if [ ! -f "$setupsh" ]; then
        print_critical "OS variant '$os_id' not supported (missing $setupsh)"
    fi
    . "$setupsh"
    print_info "System setup complete" "On a first install, a system reboot may be necessary"
    exit 0
}

# Get the job list
citbx_job_list() {
    local prefix outcmd arg
    prefix='[^\.]'
    outcmd='print $0'
    if ! arglist=$(getopt -o "f:p:s" -n "citbx_list " -- "$@"); then
        print_critical "Usage citbx_list: [options]" \
            "        -f <val>  Output gawk command (default: 'print $0')" \
            "        -s        Suffix list (same as -f 'printf(\" %s\", f[1]);')" \
            "        -p <val>  Prefix string"
    fi
    eval set -- "$arglist";
    while true; do
        arg=$1
        shift
        case "$arg" in
            -f) outcmd=$1;  shift;;
            -p) prefix=$1;  shift;;
            -s) outcmd='printf(" %s", f[1]);';;
            --) break;;
            *)  print_critical "Fatal error";;
        esac
    done
    gitlab_ci_query -r 'paths | select(.[-1] == "script") | .[0]' \
        | gawk 'match($0, /^'"$prefix"'(.*)$/, f) {'"$outcmd"'}'
}

# put YAML variables indo CITBX_YAML_SCRIPT_ELTS
gitlab_ci_variables() {
    local node=$1
    local line
    test -n "$node" \
        || print_critical "Usage: gitlab_ci_variables <node path>"
    local node_type="$(gitlab_ci_query -r "$node | type")"
    case "$node_type" in
        null)
            return 1
            ;;
        object)
            for k in $(gitlab_ci_query -r "$node | keys[]"); do
                if [ "$(gitlab_ci_query -r "${node}.$k | type")" != "string" ]; then
                    print_critical "Invalid $node variable: $k"
                fi
                line="$k=$(gitlab_ci_query "${node}.$k")"
                case "$k" in
                    GIT_SUBMODULE_STRATEGY)
                    eval "$line"
                    ;;
                esac
                CITBX_YAML_VARIABLE_ELTS+=("$line")
            done
            ;;
        *)
            print_critical "Invalid $node type"
            ;;
    esac
}

# put YAML (array or string) node script content indo CITBX_YAML_SCRIPT_ELTS
gitlab_ci_script() {
    local node=$1
    local line
    test -n "$node" \
        || print_critical "Usage: gitlab_ci_script <node path>"
    local script_type="$(gitlab_ci_query -r "$node | type")"
    case "$script_type" in
        null)
            return 1
            ;;
        string)
            CITBX_YAML_SCRIPT_ELTS+=(gitlab_ci_query -r "${node}")
            ;;
        array)
            for i in $(seq 0 $(($(gitlab_ci_query "$node | length")-1))); do
                line=$(gitlab_ci_query -r "${node}[$i]")
                if [ "$(gitlab_ci_query -r "${node}[$i] | type")" != "string" ]; then
                    print_critical "Invalid $node line: $line"
                fi
                CITBX_YAML_SCRIPT_ELTS+=("$line")
            done
            ;;
        *)
            print_critical "Invalid $node type"
            ;;
    esac
}

# Run an other job
citbx_run_ext_job() {
    local job_name=$1
    test -n "$job_name" \
        || print_critical "Usage: citbx_run_ext_job <job name>"
    print_note "Starting job $job_name"
    (
        set -e
        unset CITBX
        unset CITBX_COMMAND
        bashopts_export_opts
        export CI_JOB_NAME=$job_name
        exec $0 "$@"
    )
}

# Export an variable to the job environment
citbx_export() {
    CITBX_ENV_EXPORT_LIST+=("$@")
}

# Add docker run arguments
citbx_docker_run_add_args() {
    CITBX_DOCKER_RUN_ARGS+=("$@")
}

# Load bashopts
BASHOPTS_FILE_PATH=${BASHOPTS_FILE_PATH:-"$CITBX_ABS_DIR/3rdparty/bashopts.sh"}
if [ ! -f "$BASHOPTS_FILE_PATH" ]; then
    print_critical "Missing requered file $BASHOPTS_FILE_PATH [\$BASHOPTS_FILE_PATH]"
fi
. $BASHOPTS_FILE_PATH
# Enable backtrace dusplay on error
trap 'bashopts_exit_handle' ERR

# Set the setting file path
if [ -z "$CITBX_RC_PATH" ]; then
    CITBX_RC_PATH="/dev/null"
fi

if [ ${#CITBX_MISSING_PKGS[@]} -eq 0 ]; then
    eval "$(citbx_job_list -f 'printf("CITBX_JOB_LIST+=(\"%s\");", $0);')"
fi
bashopts_setup -n "run.sh" \
    -d "Gitlab-CI job runner tool" \
    -s "$CITBX_RC_PATH" \
    -u "$0 command [command options] [arguments...]
  => type '$0 command -h' to display the contextual help

COMMANDS:
    help            : Display this help
    setup           : Setup the environment
    update          : Update this tool (fetch the last version from https://gitlab.com/ercom/citbx4gitlab)
    <job name> from : ${CITBX_JOB_LIST[*]}"

command=$1
shift || true
case "$command" in
    ''|h|help|-h|--help)
        bashopts_diplay_help_delayed
        ;;
    setup)
        bashopts_tool_usage="$0 $command [arguments...]
  => type '$0 help' to display the global help"
        bashopts_declare -n CITBX_DOCKER_BIP -l docker-bip -v "192.168.255.254/24" \
            -t string -s -i -d "Local docker network IPV4 host adress"
        bashopts_declare -n CITBX_DOCKER_FIXED_CIDR -l docker-cdir -v "192.168.255.0/24" \
            -t string -s -i -d "Local docker network IPV4 prefix"
        bashopts_declare -n CITBX_DOCKER_DNS_LIST -l docker-dns -m add \
            -e "($(cat /etc/resolv.conf | awk '/^nameserver/ {printf(" %s", $2)}') )" \
            -t string -s -i -d "Docker DNS"
        ;;
    update)
        ;;
    *)
        pattern='\b'"$command"'\b'
        if ! [[ "${CITBX_JOB_LIST[*]}" =~ $pattern ]]; then
            print_critical "Unreconized command; type '$0 help' to display the help"
        fi
        CI_JOB_NAME=$command
        citbx_local
        JOB_EXT_FILE_NAME=${JOB_EXT_FILE_NAME:-"$CI_JOB_NAME.sh"}
        # Read Image property
        for p in '."'"$CI_JOB_NAME"'"' ''; do
            case "$(gitlab_ci_query -r "$p.image | type")" in
                object)
                    if [ "$(gitlab_ci_query -r "$p.image.name | type")" == "string" ]; then
                        CITBX_DEFAULT_DOCKER_IMAGE=$(gitlab_ci_query -r "$p.image.name")
                        if [ "$(gitlab_ci_query -r "$p.image.entrypoint | type")" == "array" ]; then
                            for i in $(seq 0 $(gitlab_ci_query -r "$p.image.entrypoint | length - 1")); do
                                CITBX_DEFAULT_DOCKER_ENTRYPOINT+=("$(gitlab_ci_query -r "$p.image.entrypoint[$i]")")
                            done
                        fi
                        break
                    fi
                    ;;
                string)
                    CITBX_DEFAULT_DOCKER_IMAGE=$(gitlab_ci_query -r "$p.image")
                    break
                    ;;
                *)
                    ;;
            esac
        done
        # Read the gitlab-ci variables
        gitlab_ci_variables ".\"variables\"" || true
        gitlab_ci_variables ".\"$CI_JOB_NAME\".\"variables\"" || true
        # Define job usage
        bashopts_tool_usage="$0 $command [arguments...]
  => type '$0 help' to display the global help"
        # Define the generic options
        bashopts_declare -n GIT_SUBMODULE_STRATEGY -l submodule-strategy \
            -d "Git submodule strategy (none, normal or recursive)" -t string -v "${GIT_SUBMODULE_STRATEGY:-none}"
        declare_opts=()
        if [ -n "$DEFAULT_CI_REGISTRY" ]; then
            declare_opts+=(-v "$DEFAULT_CI_REGISTRY")
        fi
        bashopts_declare -n CI_REGISTRY -l docker-registry -d "Docker registry" -t string -s "${declare_opts[@]}"
        unset declare_opts
        bashopts_declare -n CITBX_DOCKER_LOGIN -l docker-login -o l -d "Execute docker login" -t boolean
        bashopts_declare -n CITBX_JOB_EXECUTOR -l job-executor -o e \
            -d "Job executor type (only docker or shell is sypported yet)" -t string \
            -v "$(test -n "$CITBX_DEFAULT_DOCKER_IMAGE" && echo "docker" || echo "shell" )"
        bashopts_declare -n CITBX_DOCKER_IMAGE -l docker-image -d "Docker image name" -t string \
            -e "\"$CITBX_DEFAULT_DOCKER_IMAGE\""
        bashopts_declare -n CITBX_DOCKER_ENTRYPOINT -l docker-entrypoint -d "Docker entrypoint" -t string -m add \
            -e "$(bashopts_get_def CITBX_DEFAULT_DOCKER_ENTRYPOINT)"
        bashopts_declare -n CITBX_UID -l uid -t number \
            -d "Start this script as a specific uid (0 for root)" -v "$(id -u)"
        bashopts_declare -n CITBX_RUN_SHELL -o s -l run-shell -t boolean \
            -d "Run a shell instead of run the default command (override CITBX_COMMAND option)"
        bashopts_declare -n CITBX_JOB_SHELL -l shell -t string -v "$CITBX_DEFAULT_JOB_SHELL" \
            -d "Use a specific shell to run the job"
        CITBX_DOCKER_USER=${CITBX_DOCKER_USER:-root}

        # Load job 
        module_handler_list="define setup"
        if [ -f "$CITBX_ABS_DIR/run.d/${JOB_EXT_FILE_NAME}" ]; then
            . $CITBX_ABS_DIR/run.d/${JOB_EXT_FILE_NAME}
            citbx_register_handler "job" "define"
            citbx_register_handler "job" "setup"
        fi
        for hook in $citbx_job_stage_define; do
            $hook
        done
        ;;
esac

# Parse arguments
bashopts_parse_args "$@"

# Process argument
bashopts_process_opts

# check the environment
citbx_check_env $(test "$command" != "setup" || echo "true")

if [ "$command" == "update" ]; then
    fetch_file() {
        local file=$1
        local tg=${2:-"$CITBX_ABS_DIR/$1"}
        CITBX_REMOTE_URL=https://gitlab.com/ercom/citbx4gitlab/raw/master/tools/gitlab-ci
        print_note "Fetching file $CITBX_REMOTE_URL/$file..."
        curl -fSsLo $tg $CITBX_REMOTE_URL/$file \
            || print_warning "Unable to fetch file $CITBX_REMOTE_URL/$file"
    }
    fetch_file run.sh
    chmod +x $CITBX_ABS_DIR/run.sh
    fetch_file env-setup/ubuntu.sh
    fetch_file 3rdparty/bashopts.sh $BASHOPTS_FILE_PATH
    print_info "Update done!"
    exit 0
fi

if [ "$(gitlab_ci_query -r '."'"$CI_JOB_NAME"'".script | type')" == "null" ]; then
    print_critical "Unable to find a valid job with tne name \"$CI_JOB_NAME\" in the .gitlab-ci.yml"
fi

# Login to the registry if needed
if [ -n "$CI_REGISTRY" ] \
    && ( [ -z "$(jq -r '."auths"."'$CI_REGISTRY'"."auth"' $HOME/.docker/config.json 2> /dev/null)" ] \
    || [ "$CITBX_DOCKER_LOGIN" == "true" ] ); then
    print_info "You are not authenticated against the gitlab docker registry" \
        "> Please enter your gitlab user id and password:"
    docker login $CI_REGISTRY
fi

# Run the job setup hooks
for hook in $citbx_job_stage_setup; do
    $hook
done

# Compute commands from before_script script and after_script
CITBX_JOB_SCRIPT='
print_info() {
    printf "\e[1m\e[92m%s\e[0m\n" "$@"
}
print_error() {
    printf "\e[1m\e[91m%s\e[0m\n" "$@"
}
print_cmd() {
    printf "\e[1m\e[92m$ %s\e[0m\n" "$@"
}
__job_exit_code__=0
(
'
for line in "${CITBX_YAML_VARIABLE_ELTS[@]}"; do
    CITBX_JOB_SCRIPT="$CITBX_JOB_SCRIPT
$line || exit \$?
"
done
gitlab_ci_script ".\"$CI_JOB_NAME\".\"before_script\"" \
    || gitlab_ci_script ".\"before_script\"" \
    || true
gitlab_ci_script ".\"$CI_JOB_NAME\".\"script\"" \
    || print_critical "script $CI_JOB_NAME.script node nor found!"
for line in "${CITBX_YAML_SCRIPT_ELTS[@]}"; do
    CITBX_JOB_SCRIPT="$CITBX_JOB_SCRIPT
print_cmd $(bashopts_get_def line)
$line || exit \$?
"
done
CITBX_JOB_SCRIPT="$CITBX_JOB_SCRIPT"'
) || __job_exit_code__=$?
'
unset CITBX_YAML_SCRIPT_ELTS
if gitlab_ci_script ".\"$CI_JOB_NAME\".\"after_script\"" \
    || gitlab_ci_script ".\"after_script\""; then

    CITBX_JOB_SCRIPT="$CITBX_JOB_SCRIPT"'
print_info "Running after script..."
'
    for line in "${CITBX_YAML_SCRIPT_ELTS[@]}"; do
        CITBX_JOB_SCRIPT="$CITBX_JOB_SCRIPT
print_cmd $(bashopts_get_def line)
$line
"
    done
fi
CITBX_JOB_SCRIPT="$CITBX_JOB_SCRIPT"'
if [ $__job_exit_code__ -eq 0 ]; then
    print_info "Job succeeded"
else
    print_error "ERROR: Job failed: exit code $__job_exit_code__"
fi
exit $__job_exit_code__
'
CITBX_JOB_SCRIPT="'"${CITBX_JOB_SCRIPT//\'/\'\\\'\'}"'"

# Fetch git submodules
if [ -n "$GIT_SUBMODULE_STRATEGY" ] && [ "$GIT_SUBMODULE_STRATEGY" != "none" ]; then
    GIT_SUBMODULE_ARGS=()
    case "$GIT_SUBMODULE_STRATEGY" in
        normal)
            ;;
        recursive)
            GIT_SUBMODULE_ARGS+=("--recursive")
            ;;
        *)
            print_critical "Invalid value for GIT_SUBMODULE_STRATEGY: $GIT_SUBMODULE_STRATEGY"
            ;;
    esac
    print_info "Fetching git submodules..."
    git submodule sync "${GIT_SUBMODULE_ARGS[@]}" > /dev/null
    git submodule update --init "${GIT_SUBMODULE_ARGS[@]}"
fi

# Git SHA1
CI_COMMIT_REF_NAME=${CI_COMMIT_REF_NAME:-$(cd $CI_PROJECT_DIR && git rev-parse --abbrev-ref HEAD)}
CITBX_DOCKER_RUN_ARGS+=(-e CI_COMMIT_REF_NAME="$CI_COMMIT_REF_NAME")

# Add variable to the environment list
CITBX_ENV_EXPORT_LIST+=(CI_JOB_NAME CI_REGISTRY CI_PROJECT_DIR)

case "$CITBX_JOB_EXECUTOR" in
    shell)
        print_info "Running the job $CI_JOB_NAME into the shell $CITBX_JOB_SHELL..."
        (
            unset CITBX
            export GITLAB_CI=true
            for e in ${CITBX_ENV_EXPORT_LIST[@]}; do
                export $e
            done
            eval "$CITBX_JOB_SHELL -c $CITBX_JOB_SCRIPT"
        )
        ;;
    docker)
        # Setup docker environment
        if [ -z "$CITBX_DOCKER_IMAGE" ] || [ "$CITBX_DOCKER_IMAGE" == "null" ]; then
            print_critical "No image property found in .gitlab-ci.yml for the job $CI_JOB_NAME"
        fi
        if [ -f "$HOME/.docker/config.json" ]; then
            CITBX_DOCKER_RUN_ARGS+=(-v $HOME/.docker/config.json:/root/.docker/config.json:ro)
        fi
        CITBX_JOB_SHELL=${CITBX_JOB_SHELL:-"/bin/sh"}
        if [ "$CITBX_UID" -eq 0 ] || [ "$CITBX_DOCKER_USER" != "root" ]; then
            if [ "$CITBX_RUN_SHELL" == "true" ]; then
                CITBX_COMMANDS=$CITBX_JOB_SHELL
            else
                CITBX_COMMANDS="$CITBX_JOB_SHELL -c $CITBX_JOB_SCRIPT"
            fi
        else
            if [ -f "$HOME/.docker/config.json" ]; then
                CITBX_DOCKER_RUN_ARGS+=(-v $HOME/.docker/config.json:$HOME/.docker/config.json:ro)
            fi
            CITBX_COMMANDS="
                useradd -o -u $CITBX_UID -s /bin/sh -d $HOME -M ci-user;
                chown $CITBX_UID:$CITBX_UID $HOME
                for g in adm plugdev docker; do usermod -a -G \$g ci-user 2> /dev/null || true; done;
                if [ -f /etc/sudoers ]; then
                    sed -i \"/^ci-user /d\" /etc/sudoers;
                    echo \"ci-user ALL=(ALL) NOPASSWD:ALL\" >> /etc/sudoers;
                fi;
                su ci-user -s $CITBX_JOB_SHELL $( test "$CITBX_RUN_SHELL" == "true" \
                                || echo "-c $CITBX_JOB_SCRIPT" );
            "
        fi

        print_info "Running the job $CI_JOB_NAME into the $CITBX_DOCKER_IMAGE docker container..."

        if [ -n "$CITBX_DOCKER_USER" ]; then
            CITBX_DOCKER_RUN_ARGS+=(-u "$CITBX_DOCKER_USER")
        fi

        # Compute the environment variables
        for e in ${CITBX_ENV_EXPORT_LIST[@]}; do
            CITBX_DOCKER_RUN_ARGS+=(-e $e="${!e}")
        done

        CITBX_PRE_COMMANDS=()
        # Entrypoint override management
        if [ -n "$CITBX_DOCKER_ENTRYPOINT" ]; then
            CITBX_DOCKER_RUN_ARGS+=(--entrypoint "$CITBX_DOCKER_ENTRYPOINT")
            for e in "${CITBX_DOCKER_ENTRYPOINT[@]:1}"; do
                CITBX_PRE_COMMANDS+=("$e")
            done
        fi

        # Run the docker
        exec docker run --rm -ti -e CI=true -e GITLAB_CI=true -v /var/run/docker.sock:/var/run/docker.sock \
            -v "$CI_PROJECT_DIR:$CI_PROJECT_DIR:rw" -w "$CI_PROJECT_DIR" "${CITBX_DOCKER_RUN_ARGS[@]}" \
            -e DOCKER_RUN_EXTRA_ARGS="$(bashopts_get_def bashopts_extra_args)" \
            "${bashopts_extra_args[@]}" $CITBX_DOCKER_IMAGE "${CITBX_PRE_COMMANDS[@]}" sh -c "$CITBX_COMMANDS"
        exit 1
        ;;
    *)
        print_critical "Invalid or unsupported '$CITBX_JOB_EXECUTOR' executor"
        ;;
esac
