-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 3.0 (quilt)
Source: rats
Binary: rats
Architecture: any
Version: 2.4-1
Maintainer: Emeric Verschuur <emeric@mbedsys.org>
Homepage: http://www.securesoftware.com/
Standards-Version: 3.9.6
Vcs-Browser: https://gitlab.com/ercom/rats
Vcs-Git: https://gitlab.com/ercom/rats.git
Build-Depends: debhelper (>= 9), autotools-dev, libexpat1-dev
Package-List:
 rats deb unknown optional arch=any
Checksums-Sha1:
 1063210dbad5bd9f287b7b80bd7e412a63ae1792 393114 rats_2.4.orig.tar.gz
 db3dbe0ecaac5c97ab7c2e98903a52e4ecca4468 2336 rats_2.4-1.debian.tar.xz
Checksums-Sha256:
 2163ad111070542d941c23b98d3da231f13cf065f50f2e4ca40673996570776a 393114 rats_2.4.orig.tar.gz
 d202f43c0b82d031ee2b0fa596ec8799bfd9b358dab98c7784577b75fb877c40 2336 rats_2.4-1.debian.tar.xz
Files:
 86afc955a26811d0d631e69113a75368 393114 rats_2.4.orig.tar.gz
 9b5772dd182dc1b89d3a1369c5c1e13f 2336 rats_2.4-1.debian.tar.xz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1

iQIcBAEBAgAGBQJZpXm3AAoJEC9j9yqe9QK8TekP/0ZlSG6eO74pz2prE4wDOGpr
PGMGgT9LtJ2ycj5QIXWqvr8UItSA9HrbxSep+XV3wW1xVpLC4FYrV30BTFVsmE+p
fVddnRQmIEj9bKBWsDdlxffX/btcXI+CpZhlpHEh6jEfnPeUiLthfpWHagLRqxnm
HJ2/e3A+l2fRxFRHwn5meA0GFlN/RQN3zMuJM2fOXRbSmqnsxxt5gr1IDGdv8UqS
8mbM7PX+90qkfs+sttvdR1kOHR8x6JKaiC7OpDP7ZAiVkG/P6Ojh0DSl9AsaZuOR
HmXkzmfJoanRYRUDRM6QOdGdS42PX6g/aO2N50y1vNlykowYMUbxxlu1UVlTlHaG
2lq8jr9hSdzPSkX1O+v450urCK1RKv8qZNXuGC+7S3tp7gMO/MJD1K4qO5qf02DV
VA5/nJNe5lRH0LN7TqrCmLEEn2WkxAhvcuS/sWait8PRtGwlzbekcqy/snrMYDmD
NS6fwx1Xk7HlkymLhtPsfhppvQgvv0HfWlXxieYHhDUuQ+Mahz6oq9A8EhWW0aLc
sA9aVAGTa6pzvl69WOvLqgytrXpoap/n+Uba3jEmqllZI8KznYa/u3vIxi2CIukp
SQNsZijP2uCQgg3LikAofhBD2V8bHsRih9ox01vvKwefaxBXoHtkvmNbw2wgst7N
eBYGR7ikxhUbfcbNn1Tx
=8Ji4
-----END PGP SIGNATURE-----
